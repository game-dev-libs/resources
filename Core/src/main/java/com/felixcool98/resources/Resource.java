package com.felixcool98.resources;

import com.badlogic.gdx.files.FileHandle;

public class Resource {
	private FileHandle handle;
	
	
	protected Resource(FileHandle handle) {
		this.handle = handle;
	}
	
	
	public FileHandle getHandle() {
		return handle;
	}
	
	public Directory getParent() {
		return Resources.getDirectory(getHandle().parent());
	}
	
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " " + handle.name();
	}
}
