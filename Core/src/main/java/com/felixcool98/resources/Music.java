package com.felixcool98.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music.OnCompletionListener;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;
import com.felixcool98.resources.ResourceManager.ResourceHandler;
import com.felixcool98.utility.Checks;

/**
 * for further information look into {@link com.felixcool98.resources.badlogic.gdx.audio.Music}
 * 
 * @author felixcool98
 *
 */
public class Music extends Resource implements Disposable {
	private com.badlogic.gdx.audio.Music CURRENT;
	/**change to false if you want to dispose the music yourself**/
	public static boolean AUTO_DISPOSE = true;
	
	
	private com.badlogic.gdx.audio.Music music;
	
	
	protected Music(FileHandle handle) {
		super(handle);
	}

	
	public com.badlogic.gdx.audio.Music getMusic(){
		if(music == null) {
			music = Gdx.audio.newMusic(getHandle());
		}
		
		return music;
	}
	

	@Override
	public void dispose() {
		if(music == null)
			return;
		
		music.dispose();
		
		music = null;
	}
	
	private void prepare() {
		if(!AUTO_DISPOSE)
			return;
		
		if(music == null && CURRENT != null)
			CURRENT.dispose();
		
		CURRENT = getMusic();
	}
	
	
	//======================================================================
	// overrides
	//======================================================================
	public void play() {
		prepare();
		
		getMusic().play();
	}
	public void setLooping(boolean looping) {
		prepare();
		
		getMusic().setLooping(looping);
	}
	public void setOnCompletionListener(OnCompletionListener listener) {
		prepare();
		
		getMusic().setOnCompletionListener(listener);
	}
	public void setPan(float pan, float volume) {
		prepare();
		
		getMusic().setPan(pan, volume);
	}
	public void setPosition(float position) {
		prepare();
		
		getMusic().setPosition(position);
	}
	public void setVolume(float volume) {
		prepare();
		
		getMusic().setVolume(volume);
	}
	public void stop() {
		prepare();
		
		getMusic().stop();
	}
	public void pause() {
		prepare();
		
		getMusic().pause();
	}
	
	
	public static class MusicResourceHandler implements ResourceHandler<Music> {
		@Override
		public boolean valid(FileHandle handle) {
			if(Checks.equalsAny(handle.extension(), "ogg", "mp3", "wav"))
				return true;
			
			return false;
		}

		@Override
		public Music createResource(FileHandle handle) {
			return new Music(handle);
		}

		@Override
		public Class<Music> getResouceClass() {
			return Music.class;
		}
	}
}
