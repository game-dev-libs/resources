package com.felixcool98.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;
import com.felixcool98.resources.ResourceManager.ResourceHandler;
import com.felixcool98.utility.Checks;

/**
 * for further information look into {@link com.felixcool98.resources.badlogic.gdx.audio.Sound}
 * 
 * @author felixcool98
 *
 */
public class Sound extends Resource implements Disposable {
	private com.badlogic.gdx.audio.Sound sound;
	
	
	protected Sound(FileHandle handle) {
		super(handle);
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	public com.badlogic.gdx.audio.Sound getSound(){
		if(sound == null) {
			sound = Gdx.audio.newSound(getHandle());
		}
		
		return sound;
	}
	
	
	//======================================================================
	// other
	//======================================================================
	private SoundInstance createSoundInstance(long id) {
		return new SoundInstance(getSound(), id);
	}
	
	
	//======================================================================
	// wrapper
	//======================================================================
	
	//loop
	public SoundInstance loop() {
		return createSoundInstance(getSound().loop());
	}
	public SoundInstance loop(float volume) {
		return createSoundInstance(getSound().loop(volume));
	}
	public SoundInstance loop(float volume, float pitch, float pan) {
		return createSoundInstance(getSound().loop(volume, pitch, pan));
	}
	
	//play
	public SoundInstance play() {
		return createSoundInstance(getSound().play());
	}
	public SoundInstance play(float volume) {
		return createSoundInstance(getSound().play(volume));
	}
	public SoundInstance play(float volume, float pitch, float pan) {
		return createSoundInstance(getSound().play(volume, pitch, pan));
	}
	
	//pause
	public void pause() {
		getSound().pause();
	}
	public void pause(long id) {
		getSound().pause(id);
	}
	
	//resume
	public void resume() {
		getSound().resume();
	}
	public void resume(long id) {
		getSound().resume(id);
	}
	
	//stop
	public void stop() {
		getSound().stop();
	}
	public void stop(long id) {
		getSound().stop(id);
	}
	
	
	//======================================================================
	// overrides
	//======================================================================
	@Override
	public void dispose() {
		if(sound == null)
			return;
		
		sound.dispose();
		
		sound = null;
	}
	
	
	//======================================================================
	// nested classes
	//======================================================================
	public static class SoundInstance {
		private com.badlogic.gdx.audio.Sound sound;
		private long id;
		
		
		public SoundInstance(com.badlogic.gdx.audio.Sound sound, long id) {
			this.sound = sound;
			this.id = id;
		}
		
		
		//======================================================================
		// getter
		//======================================================================
		public long getID() {
			return id;
		}
		
		
		//======================================================================
		// wrapper
		//======================================================================
		
		//pause
		public void pause() {
			sound.pause(getID());
		}
		
		//resume
		public void resume() {
			sound.resume(getID());
		}
		
		//stop
		public void stop() {
			sound.stop(getID());
		}
	}
	
	
	public static class SoundResourceHandler implements ResourceHandler<Sound> {
		@Override
		public boolean valid(FileHandle handle) {
			if(Checks.equalsAny(handle.extension(), "ogg", "mp3", "wav"))
				return true;
			
			return false;
		}

		@Override
		public Sound createResource(FileHandle handle) {
			return new Sound(handle);
		}

		@Override
		public Class<Sound> getResouceClass() {
			return Sound.class;
		}
	}
}
