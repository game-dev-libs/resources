package com.felixcool98.resources;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.files.FileHandle;
import com.felixcool98.gdxutility.filter.DirectoryFilter;
import com.felixcool98.gdxutility.filter.FileExtensionFilter;
import com.felixcool98.resources.ResourceManager.ResourceHandler;
import com.felixcool98.utility.filter.Filter;

public class Directory extends Resource {
	protected Directory(FileHandle handle) {
		super(handle);
	}

	
	//======================================================================
	// single resources
	//======================================================================
	public <A extends Resource> A get(String name, Class<A> classs) {
		FileHandle child = getHandle().child(name);
		
		if(child == null)
			return null;
		
		return Resources.get(child, classs);
	}
	public Resource getChild(String name) {
		FileHandle child = getHandle().child(name);
		
		if(child == null)
			return null;
		
		return Resources.getResource(child);
	}
	public Image getImage(String name) {
		return get(name, Image.class);
	}
	
	public XML getXML(String name) {
		return get(name, XML.class);
	}
	public Directory getDirectory(String name) {
		return get(name, Directory.class);
	}
	public Config getConfig(String name) {
		return get(name, Config.class);
	}
	public SML getSML(String name) {
		return get(name, SML.class);
	}
	
	
	//======================================================================
	// all resources of type
	//======================================================================
	
	//all
	
	public <T extends Resource> List<T> getAll(Filter<FileHandle> filter, Class<T> classs){
		List<T> resources = new LinkedList<>();
		
		for(FileHandle handle : getHandle().list()) {
			if(!filter.valid(handle))
				continue;
			
			
			resources.add(get(handle.name(), classs));
		}
		for(Directory directory : getDirectories()) {
			resources.addAll(directory.getAll(filter, classs));
		}
		
		return resources;
	}
	
	//Configs
	public List<Config> getConfigs(){
		return getConfigs("cfg");
	}
	public List<Config> getConfigs(String extension){
		return getAll((new FileExtensionFilter(extension)), Config.class);
	}
	
	//Directories
	public List<Directory> getDirectories(){
		List<Directory> dirs = new LinkedList<>();
		
		for(FileHandle handle : getHandle().list()) {
			if(!new DirectoryFilter().valid(handle))
				continue;
			
			dirs.add(getDirectory(handle.name()));
		}
		
		return dirs;
	}
	
	//XMLs
	public List<XML> getXMLs(){
		return getXMLs("xml");
	}
	public List<XML> getXMLs(String extension){
		return getAll((new FileExtensionFilter(extension)), XML.class);
	}
	
	//SMLs
	public List<SML> getSMLs(String extension){
		return getAll((new FileExtensionFilter(extension)), SML.class);
	}
	
	
	public static class DirectoryResourceHandler implements ResourceHandler<Directory> {
		@Override
		public boolean valid(FileHandle handle) {
			return handle.isDirectory();
		}
		@Override
		public Directory createResource(FileHandle handle) {
			return new Directory(handle);
		}
		@Override
		public Class<Directory> getResouceClass() {
			return Directory.class;
		}
	}
}
