package com.felixcool98.resources;

import com.badlogic.gdx.files.FileHandle;
import com.felixcool98.resources.ResourceManager.ResourceHandler;

public class Resources {
	private static ResourceManager MANAGER = new ResourceManager();
	
	
	//======================================================================
	// manager
	//======================================================================
	public static void setManager(ResourceManager manager) {
		MANAGER = manager;
	}
	
	
	public static ResourceManager getManager() {
		return MANAGER;
	}
	
	
	//======================================================================
	// other methods
	//======================================================================
	/**
	 * calls {@link ResourceManager#dispose()} on the {@link ResourceManager#INSTANCE} object
	 */
	public static void disposeResources() {
		MANAGER.dispose();
	}
	
	/**
	 * calls {@link ResourceManager#registerHandler(ResourceHandler)} on the {@link ResourceManager#INSTANCE} object
	 * 
	 * @param handler the handler to register
	 */
	public static void registerResourceHandler(ResourceHandler<?> handler) {
		MANAGER.registerHandler(handler);
	}
	
	
	//======================================================================
	// getting resources
	//======================================================================
	
	//general
	
	public static <T extends Resource> T get(FileHandle handle, Class<T> classs) {
		return MANAGER.get(handle, classs);
	}
	
	public static Resource getResource(FileHandle handle) {
		return MANAGER.getResource(handle);
	}
	
	//specific
	
	public static Directory getDirectory(FileHandle handle) {
		return MANAGER.getDirectory(handle);
	}
	public static SML getSML(FileHandle handle) {
		return MANAGER.getSML(handle);
	}
	
	public static Image getImage(FileHandle handle) {
		return MANAGER.getImage(handle);
	}
	public static Image getImage(String path) {
		return MANAGER.getImage(path);
	}
}
