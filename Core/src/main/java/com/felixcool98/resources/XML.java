package com.felixcool98.resources;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.felixcool98.resources.ResourceManager.ResourceHandler;

public class XML extends Resource {
	private Element element;
	
	
	protected XML(FileHandle handle) {
		super(handle);
	}
	
	
	public Element getElement() {
		if(element == null) {
			XmlReader reader = new XmlReader();
			element = reader.parse(getHandle());
		}
		
		return element;
	}
	

	public static class XMLResourceHandler implements ResourceHandler<XML> {
		@Override
		public XML createResource(FileHandle handle) {
			return new XML(handle);
		}
		@Override
		public boolean valid(FileHandle handle) {
			if(handle.isDirectory())
				return false;
			
			XmlReader reader = new XmlReader();
			try {
				reader.parse(handle);
			} catch (Exception e) {
				return false;
			} 
			
			return true;
		}
		@Override
		public Class<XML> getResouceClass() {
			return XML.class;
		}
	}
}
