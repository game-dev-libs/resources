package com.felixcool98.resources;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;

/**
 * Manager for obtaining resources<br>
 * will make sure that only one resource per file can exist
 * 
 * @author felixcool98
 *
 */
public class ResourceManager implements Disposable {
	
	private Map<FileHandle, Resource> resources = new HashMap<>();
	
	private Map<Class<? extends Resource>, ResourceHandler<?>> handlers = new HashMap<>();
	
	
	public Resource getResource(FileHandle handle) {
		if(!resources.containsKey(handle)) {
			handle.mkdirs();
			
			if(!handle.exists()) {
				try {
					handle.file().createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			resources.put(handle, createResource(handle));
		}
		
		return resources.get(handle);
	}
	@SuppressWarnings("unchecked")
	public <A extends Resource> A get(FileHandle handle, Class<A> classs) {
		if(!handlers.containsKey(classs))
			return null;
		
		return (A) handlers.get(classs).createResource(handle);
	}
	public Directory getDirectory(FileHandle handle) {
		return get(handle, Directory.class);
	}
	public Config getConfig(FileHandle handle) {
		return get(handle, Config.class);
	}
	public XML getXML(FileHandle handle) {
		return get(handle, XML.class);
	}
	public SML getSML(FileHandle handle) {
		return get(handle, SML.class);
	}
	
	//images
	
	public Image getImage(String path) {
		return getImage(getExistingFileHandle(path));
	}
	public Image getImage(FileHandle handle) {
		return get(handle, Image.class);
	}
	
	private Resource createResource(FileHandle handle) {
		if(handle == null)
			return null;
		
		for(ResourceHandler<?> handler : handlers.values()) {
			if(!handler.valid(handle))
				continue;
			
			return handler.createResource(handle);
		}
		
		if(handle.exists())
			return new Resource(handle);
		
		return null;
	}
	
	
	public void registerHandler(ResourceHandler<?> handler) {
		handlers.put(handler.getResouceClass(), handler);
	}
	
	
	public boolean hasResourceHandler(Class<? extends Resource> classs) {
		return handlers.containsKey(classs);
	}
	
	
	@Override
	public void dispose() {
		for(Resource resource : resources.values()) {
			if(!(resource instanceof Disposable))
				continue;
			
			((Disposable) resource).dispose();
		}
	}
	
	
	public FileHandle getExistingFileHandle(String path) {
		FileHandle handle = Gdx.files.internal(path);
		
		if(handle.exists())
			return handle;
		
		 handle = Gdx.files.local(path);
		
		if(handle.exists())
			return handle;
		
		handle = Gdx.files.external(path);
		
		if(handle.exists())
			return handle;
		
		handle = Gdx.files.absolute(path);
		
		if(handle.exists())
			return handle;
		
		handle = Gdx.files.classpath(path);
		
		if(handle.exists())
			return handle;
		
		return null;
	}
	
	
	public static interface ResourceHandler<A extends Resource> {
		public boolean valid(FileHandle handle);
		public A createResource(FileHandle handle);
		public Class<A> getResouceClass();
	}
}
