package com.felixcool98.resources;

import java.util.Collection;
import java.util.List;

import com.badlogic.gdx.files.FileHandle;
import com.felixcool98.resources.ResourceManager.ResourceHandler;
import com.felixcool98.sml.SMLReader;
import com.felixcool98.sml.Type;
import com.felixcool98.utility.multivaluemap.LinkedListMultiValueMap;

public class SML extends Resource {
	private LinkedListMultiValueMap<String, Type> types;
	
	
	protected SML(FileHandle handle) {
		super(handle);
	}

	
	public void initialize() {
		if(isInitialized())
			return;
		
		types = new LinkedListMultiValueMap<>();
		
		if(!getHandle().exists())
			return;
		
		SMLReader reader = new SMLReader();
		
		List<Type> list = reader.parse(getHandle().reader());
		
		for(Type type : list) {
			types.put(type.getName(), type);
		}
	}
	
	
	public boolean isInitialized() {
		return types != null;
	}
	
	public Type getType(String name) {
		if(!isInitialized())
			initialize();
		
		if(types.get(name).isEmpty())
			return null;
		
		return types.get(name).getFirst();
	}
	public List<Type> getTypes(String name){
		if(!isInitialized())
			initialize();
		
		return types.get(name);
	}
	public List<Type> getTypes(){
		if(!isInitialized())
			initialize();
		
		return types.values();
	}
	
	
	public Type addType(String name) {
		Type type = new Type(name);
		
		addType(type);
		
		return type;
	}
	public void addType(Type type) {
		if(!isInitialized())
			initialize();
		
		types.put(type.getName(), type);
	}
	
	
	public void clear() {
		if(!isInitialized())
			initialize();
		
		types.clear();
	}
	public void remove(Collection<Type> types) {
		for(Type type : types) {
			this.types.remove(type);
		}
	}
	
	
	public void save() {
		FileHandle handle = getHandle();
		
		String string = "";
		
		for(Type type : types.values()) {
			string += type.toFormatString() + "\n";
		}
		
		handle.writeString(string, false);
	}
	
	
	public static class SMLResourceHandler implements ResourceHandler<SML> {
		@Override
		public boolean valid(FileHandle handle) {
			if(handle.isDirectory())
				return false;
			
			SMLReader reader = new SMLReader();
			try {
				reader.parse(handle.reader());
			} catch (Exception e) {
				return false;
			} 
			
			return true;
		}

		
		@Override
		public SML createResource(FileHandle handle) {
			return new SML(handle);
		}
		

		@Override
		public Class<SML> getResouceClass() {
			return SML.class;
		}
		
	}
}
