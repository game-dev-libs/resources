package com.felixcool98.resources;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Disposable;
import com.felixcool98.resources.ResourceManager.ResourceHandler;

public class Image extends Resource implements Disposable {
	private Texture texture;
	
	
	protected Image(FileHandle handle) {
		super(handle);
	}

	
	public Texture getTexture() {
		if(texture == null)
			texture = new Texture(getHandle());
		
		return texture;
	}
	public TextureRegion createTextureRegion() {
		return new TextureRegion(getTexture());
	}
	
	
	@Override
	public void dispose() {
		texture.dispose();
	}
	
	
	public static class ImageResouceHandler implements ResourceHandler<Image>{
		@Override
		public boolean valid(FileHandle handle) {
			if(handle.isDirectory())
				return false;
			
			String extension = handle.extension();
			
			return extension.equals("png") || extension.equals("jpg");
		}
		@Override
		public Image createResource(FileHandle handle) {
			return new Image(handle);
		}
		@Override
		public Class<Image> getResouceClass() {
			return Image.class;
		}
	}
}
