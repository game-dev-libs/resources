package com.felixcool98.resources;

import java.io.IOException;
import java.util.Properties;

import com.badlogic.gdx.files.FileHandle;
import com.felixcool98.resources.ResourceManager.ResourceHandler;

public class Config extends Resource {
	private Properties properties;
	
	
	protected Config(FileHandle handle) {
		super(handle);
		
		this.properties = new Properties();
		
		try {
			handle.writeString("", true);
			
			properties.load(handle.read());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	//=====================================================================
	// put stuff into properties
	//=====================================================================
	public void put(String name, String string) {
		properties.setProperty(name, string);
	}
	public void put(String name, int number) {
		properties.setProperty(name, "" + number);
	}
	public void put(String name, float number) {
		properties.setProperty(name, "" + number);
	}
	public void put(String name, boolean bool) {
		properties.setProperty(name, "" + bool);
	}
	
	
	//=====================================================================
	// get stuff from properties
	//=====================================================================
	public String getString(String name) {
		return properties.getProperty(name);
	}
	public int getInteger(String name) {
		return Integer.valueOf(properties.getProperty(name));
	}
	public float getFloat(String name) {
		return Float.valueOf(properties.getProperty(name));
	}
	public boolean getBoolean(String name) {
		return Boolean.valueOf(properties.getProperty(name));
	}
	public String getString(String name, String defaultValue) {
		return properties.getProperty(name, defaultValue);
	}
	public int getInteger(String name, int defaultValue) {
		return Integer.valueOf(properties.getProperty(name, "" + defaultValue));
	}
	public float getFloat(String name, float defaultValue) {
		return Float.valueOf(properties.getProperty(name, "" + defaultValue));
	}
	public boolean getBoolean(String name, boolean defaultValue) {
		return Boolean.valueOf(properties.getProperty(name, "" + defaultValue));
	}
	
	
	//=====================================================================
	// checks
	//=====================================================================
	public boolean contains(String name) {
		return properties.getProperty(name) != null;
	}
	
	
	public void safe() {
		try {
			properties.store(getHandle().write(false), "");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static class ConfigResourceHandler implements ResourceHandler<Config> {
		@Override
		public Config createResource(FileHandle handle) {
			return new Config(handle);
		}
		@Override
		public boolean valid(FileHandle handle) {
			if(handle.isDirectory())
				return false;
			
			try {
				handle.writeString("", true);
				
				new Properties().load(handle.read());
			} catch (IOException e) {
				return false;
			}
			
			return true;
		}
		@Override
		public Class<Config> getResouceClass() {
			return Config.class;
		}
	}
}
