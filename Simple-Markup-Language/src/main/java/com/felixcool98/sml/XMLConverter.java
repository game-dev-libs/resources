package com.felixcool98.sml;

import java.util.List;
import java.util.Map.Entry;

import com.badlogic.gdx.utils.XmlReader.Element;

public class XMLConverter {
	private ArgumentSaveType argumentSaveType;
	
	
	public XMLConverter() {
		this(ArgumentSaveType.NUMBERED_WITH_VALUE);
	}
	public XMLConverter(ArgumentSaveType argumentSaveType) {
		this.argumentSaveType = argumentSaveType;
	}
	
	
	public Element toXML(List<Type> types, String elementName) {
		Element element = new Element(elementName, null);
		
		for(Type type : types) {
			element.addChild(toXML(type, element));
		}
		
		return element;
	}
	private Element toXML(Type type, Element parent) {
		Element element = new Element(type.getName(), parent);
		
		int i = 0;
		for(String argument : type.getArguments()) {
			switch (argumentSaveType) {
				case NAME_WITH_NO_VALUE:
					element.setAttribute(argument, "");
					break;
				case NUMBERED_WITH_VALUE:
					element.setAttribute("" + i, argument);
					break;
	
				default:
					break;
			}
			
			i++;
		}
		for(Entry<String, String> entry : type.getArgumentMapEntries()) {
			element.setAttribute(entry.getKey(), entry.getValue());
		}
		
		for(Attribute attribute : type.getAttributes()) {
			element.addChild(toXML(attribute, element));
		}
		
		return element;
	}
	private Element toXML(Attribute attribute, Element parent) {
		Element element = new Element(attribute.getName(), parent);
		
		if(attribute.hasText()) {
			element.setText(attribute.getText());
		}
		
		for(Attribute child : attribute.getAttributes()) {
			element.addChild(toXML(child, element));
		}
		
		return element;
	}
	
	
	/**
	 * enum for changing the converters behavior on argument tags
	 * 
	 * @author felixcool98
	 *
	 */
	public static enum ArgumentSaveType {
		/**will turn <b>name</b> into <b>name=""</b>*/
		NAME_WITH_NO_VALUE,
		/**will turn <b>name</b> into <b>0="name"</b> the number is the order in the original SML Type*/
		NUMBERED_WITH_VALUE;
	}
}
