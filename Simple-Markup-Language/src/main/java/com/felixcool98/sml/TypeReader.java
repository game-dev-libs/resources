package com.felixcool98.sml;

import java.util.LinkedList;
import java.util.List;

public abstract class TypeReader<T> {
	
	public abstract boolean valid(Type type);
	public abstract T create(Type type);
	
	
	public static abstract class TypeReaderImpl<T> extends TypeReader<T> {
		private List<IAttributeReader<?>> attributeReaders = new LinkedList<>();
		
		
		//======================================================================
		// setter / adding
		//======================================================================
		public void add(IAttributeReader<?>reader) {
			attributeReaders.add(reader);
		}
		
		
		//======================================================================
		// getter
		//======================================================================
		public List<IAttributeReader<?>> getAttributeReaders(){
			return attributeReaders;
		}
		
		
		public IAttributeReader<?> getAttributeReader(Attribute attribute){
			for(IAttributeReader<?> reader : getAttributeReaders()) {
				if(reader.valid(attribute))
					return reader;
			}
			
			return null;
		}
	}
}
