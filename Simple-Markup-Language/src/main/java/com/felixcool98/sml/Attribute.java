package com.felixcool98.sml;

import java.util.List;

import com.felixcool98.utility.multivaluemap.LinkedListMultiValueMap;

public class Attribute {
	private String name;
	
	private String text;
	
	private LinkedListMultiValueMap<String, Attribute> attributes = new LinkedListMultiValueMap<>();
	
	private Attribute parent;
	
	
	public Attribute(String name) {
		this(name, null);
	}
	public Attribute(String name, Attribute parent) {
		this.name = name;
		this.parent = parent;
	}
	
	
	//======================================================================
	// setter / adding
	//======================================================================
	public void setName(String name) {
		this.name = name;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public void addAttribute(Attribute attribute) {
		attributes.put(attribute.getName(), attribute);
		attribute.parent = this;
	}
	
	
	//======================================================================
	// removing / clearing
	//======================================================================
	public void clearAttributes() {
		attributes.clear();
	}
	
	
	//======================================================================
	// checks
	//======================================================================
	
	public boolean hasText() {
		return getText() != null;
	}
	
	public boolean hasAttribute(String name) {
		return attributes.containsKey(name);
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	
	public String getName() {
		return name;
	}
	
	public Attribute getParent() {
		return parent;
	}
	public int getDepth() {
		if(getParent() == null)
			return 0;
		
		int depth = 0;
		
		Attribute parent = getParent();
		
		while(parent != null) {
			parent = parent.getParent();
			
			depth ++;
		}
		
		return depth;
	}
	
	//Text
	
	public String getText() {
		return text;
	}
	
	public float getTextAsFloat() {
		if(!hasText())
			return 0;
		
		return Float.parseFloat(getText().replaceAll(",", "."));
	}
	public int getTextAsInteger() {
		if(!hasText())
			return 0;
		
		return Integer.parseInt(getText());
	}
	public boolean getTextAsBoolean() {
		if(!hasText())
			return false;
		
		return Boolean.parseBoolean(getText());
	}
	
	//Attributes
	
	public Attribute getAttribute(String name) {
		if(!attributes.containsKey(name))
			return null;
		
		return attributes.get(name).getFirst();
	}
	public List<Attribute> getAttributes(String name) {
		return attributes.get(name);
	}
	public List<Attribute> getAttributes() {
		return attributes.values();
	}
	
	//AttributeText
	
	public String getText(String name, String defaultValue) {
		Attribute attribute = getAttribute(name);
		
		if(attribute == null)
			return defaultValue;
		
		return attribute.getText();
	}
	public float getFloat(String name, float defaultValue) {
		return Float.parseFloat(getText(name, "" + defaultValue).replaceAll(",", "."));
	}
	public int getInteger(String name, int defaultValue) {
		return Integer.parseInt(getText(name, "" + defaultValue));
	}
	public boolean getBoolean(String name, boolean defaultValue) {
		return Boolean.parseBoolean(getText(name, "" + defaultValue));
	}
	
	
	//======================================================================
	// overrides
	//======================================================================
	@Override
	public String toString() {
		return "Attribute " + getName();
	}
	public String toFormatString() {
		String text = "";
		
		for(int i = 0; i < getDepth() + 1; i++) {
			text += "\t";
		}
		
		text += getName();
		
		if(hasText()) {
			text += " = \"" + getText()+"\"";
		}
		
		for(Attribute attribute : attributes.values()) {
			text += System.lineSeparator() + attribute.toFormatString();
		}
		
		return text;
	}
}
