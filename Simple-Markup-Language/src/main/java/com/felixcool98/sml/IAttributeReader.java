package com.felixcool98.sml;

import java.util.LinkedList;
import java.util.List;

public interface IAttributeReader<T> {
	public boolean valid(Attribute attribute);
	
	public T process(Attribute attribute);
	
	
	public abstract static class AttributeReaderImpl<T> implements IAttributeReader<T> {
		private List<String> names = new LinkedList<>();
		
		
		public AttributeReaderImpl(String...strings) {
			for(String str : strings) {
				names.add(str);
			}
		}
		public AttributeReaderImpl() {
			
		}
		
		
		@Override
		public boolean valid(Attribute attribute) {
			for(String name : names) {
				if(name.equals(attribute.getName()))
					return true;
			}
			
			return false;
		}
	}
	
	public abstract static class AttributeReaderWithType<T, W> extends AttributeReaderImpl<T> {
		public AttributeReaderWithType(String...strings) {
			super(strings);
		}
		public AttributeReaderWithType() {
			super();
		}
		
		@Override
		public T process(Attribute attribute) {
			throw new IllegalAccessError("process without any type should never be called in AttributeReaderWithType");
		}
		public abstract T process(Attribute attribute, W type);
	}
 }
