package com.felixcool98.sml;

import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

public class SMLReader {
	private List<Type> types;
	
	private Type currentType;
	
	private Attribute currentAttribute;
	
	private State state = State.START;
	
	private String name = "";
	private String text = "";
	
	private boolean debug = false;
	private boolean debugPrintProcessedChar = true;
	private boolean debugPrintStateChange = false;
	
	private int indent = 0;
	
	private int bufferSize = 1024;
	
	private boolean skipUntilNewLine = false;
	private boolean commentStart = false;
	private boolean string = false;
	
	
	public SMLReader() {
		this(1024);
	}
	public SMLReader(int bufferSize) {
		this(false, bufferSize);
	}
	public SMLReader(boolean debug, int bufferSize) {
		this.debug = debug;
		this.bufferSize = bufferSize;
	}
	
	
	public List<Type> parse(Reader reader){
		types = new LinkedList<>();
		
		text = "";
		
		setState(State.START);
		
		char[] buffer = new char[bufferSize];
		int offset = 0;
		
		try {
			while(true) {
				int length = reader.read(buffer, offset, buffer.length);
				
				if(length != -1) {
					parse(buffer, length);
				}
				
				if(length < buffer.length)
					break;
				else if(length == -1)
					break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(currentType != null && !types.contains(currentType)) {
			types.add(currentType);
		}
		if(name.length() == 0) {
			currentType.addArgument(text.trim());
		}
		
		createNewAttribute();
		
		return types;
	}
	public List<Type> parse(char[] chars, int length){
		for(int i = 0; i < length; i++) {
			char character = chars[i];
			
			if(character == '/') {
				if(!commentStart) {
					commentStart = true;
				}else {
					skipUntilNewLine = true;
				}
			}else {
				commentStart = false;
			}
			
			if(skipUntilNewLine) {
				if(character != '\n' && character != '\r')
					continue;
				
				commentStart = false;
				
				skipUntilNewLine = false;
				
				continue;
			}
			
			executeState(state, character);
			
			if(debug && debugPrintProcessedChar) {
				String print = "";
				
				if(character != '\n' && character != '\r') {
					if(character == 't')
						print = "\\t";
					else
						print = ""+character;
				}else {
					if(character == '\n')
						print = "\\n";
					else
						print = "\\r";
				}
				
				System.out.println("current char: "+print);
			}
		}
	
		return types;
	}
	private void executeState(State state, char character) {
		if(character == '"') {
			string = !string;
			
			return;
		}
		
		if(string) {
			text += character;
			return;
		}
		
		switch(state) {
			case START:
				if(!Character.isDigit(character) && !Character.isLetter(character))
					throw new IllegalArgumentException("SML Files should always start with a type declaration");
				
				setState(State.TYPE_NAME);;
				
			case TYPE_NAME:
				if(character == '\n' || character == '\r') {
					setState(State.TYPE_READ);;
					
					break;
				}
				
				name += character;
				
				if(name.endsWith("  ")) {
					name = name.replace("  ", "");
					
					setState(State.TYPE_ARGUMENTS);
					
					setCurrentType(new Type(name));
					
					name = "";
				}
				
				break;
			case TYPE_ARGUMENTS:
				if(character == '\n' || character == '\r') {
					text = text.trim();
					
					if(text.length() != 0) {
						addArgumentToCurrentType(text);
					}
					
					setState(State.TYPE_READ);;
					
					text = "";
					
					break;
				}
				
				text += character;
				
				if(text.endsWith("  ")) {
					text.replace("  ", "");
					
					addArgumentToCurrentType(text);
					
					text = "";
				}
				
				break;
			case TYPE_READ:
				if(currentType == null)
					currentType = new Type(name);
				
				if(currentType != null && !types.contains(currentType))
					types.add(currentType);
				
				if(character == '\n' || character == '\r')
					break;
				
				if(Character.isDigit(character) || Character.isLetter(character)) {
					setState(State.START);
					
					name += character;
					
					break;
				}
				
				if(character == '\t') {
					setState(State.ATTRIBUTE_NAME);
					
					addIndent();
					
					name = "";
					text = "";
				}
				break;
			case ATTRIBUTE_NAME:
				if(character == '\n' || character == '\r') {
					setState(State.ATTRIBUTE_END);
					
					break;
				}else if(character == '=') {
					setState(State.ATTRIBUTE_TEXT);
					
					break;
				}else {
					name += character;
					
					break;
				}
				
			case ATTRIBUTE_TEXT:
				if(character == '\n' || character == '\r') {
					setState(State.ATTRIBUTE_END);
				}else {
					text += character;
					
					break;
				}
				
			case ATTRIBUTE_END:
				createNewAttribute();
				
				setState(State.GET_LINE_STATE);
				
				name = "";
				text = "";
				
				break;
			case GET_LINE_STATE:
				if(character == '\t') {
					addIndent();
					
					break;
				}else if(character == '\r' || character == '\n') {
					resetIndent();
					
					break;
				}else if(Character.isLetter(character) || Character.isDigit(character)) {
					if(indent > 0) {
						setState(State.ATTRIBUTE_NAME);
						
						name += character;
						
						break;
					}else {
						setState(State.TYPE_NAME);
						
						name += character;
					}
				}
				
				break;
			
			default:
				break;
		}
	}
	
	private void addIndent() {
		if(debug)
			System.out.println("indented");
		
		indent ++;
	}
	private void resetIndent() {
		if(debug)
			System.out.println("indent reset");
		
		indent = 0;
	}
	
	private void setState(State state) {
		if(state == State.GET_LINE_STATE && this.state != State.GET_LINE_STATE) {
			resetIndent();
			text = "";
			
			if(debug)
				System.out.println("reset text");
		}
		
		this.state = state;
		
		if(debug && debugPrintStateChange)
			System.out.println("changed state to: " + state.toString());
	}
	
	private void setCurrentType(Type type) {
		currentType = type;
		
		if(debug)
			System.out.println("set current type to: " + currentType.toString());
	}
	private void addArgumentToCurrentType(String argument) {
		if(argument.contains("=")) {
			String[] str = argument.split("=");
			
			String name = str[0].trim();
			String value = str[1].trim();
			
			currentType.addArgument(name, value);
			
			if(debug)
				System.out.println("added argument to current type: " + name + " " + value);
		}else {
			currentType.addArgument(argument);
			
			if(debug)
				System.out.println("added argument to current type: " + argument);
		}
	}
	
	private void createNewAttribute() {
		if(name == null || name.length() == 0)
			return;
		
		name = name.trim();
		
		if(indent == 1) {
			//in type
			setCurrentAttribute(new Attribute(name));
			
			currentType.addAttribute(currentAttribute);
		}else if(currentAttribute.getDepth() + 1 < indent) {
			//in current attribute
			
			Attribute oldAttribute = currentAttribute;
			Attribute newAttribute = new Attribute(name, oldAttribute);
			
			oldAttribute.addAttribute(newAttribute);
			
			setCurrentAttribute(newAttribute);
		}else if(currentAttribute.getDepth() + 1 >= indent) {
			//in current attributes parent
			Attribute parent = currentAttribute;
			
			while(parent != null && parent.getParent() != null && parent.getDepth() + 1 >= indent)
				parent = parent.getParent();
			
			Attribute newAttribute = new Attribute(name, parent);
			parent.addAttribute(newAttribute);
			
			setCurrentAttribute(newAttribute);
		}
		
		setTextOfCurrentAttribute(text);
	}
	private void setCurrentAttribute(Attribute attribute) {
		currentAttribute = attribute;
		
		if(debug)
			System.out.println("set current attribute to: " + currentAttribute.toString());
	}
	private void setTextOfCurrentAttribute(String text) {
		if(text == null || text.length() == 0)
			return;
		
		text = text.trim();
		
		currentAttribute.setText(text);
		
		if(debug)
			System.out.println("set current attributes text to: " + currentAttribute.getText());
	}
	
	
	private enum State {
		START,
		TYPE_NAME,
		TYPE_ARGUMENTS,
		TYPE_READ,
		ATTRIBUTE_NAME,
		ATTRIBUTE_TEXT,
		ATTRIBUTE_END,
		GET_LINE_STATE;
	}
}
