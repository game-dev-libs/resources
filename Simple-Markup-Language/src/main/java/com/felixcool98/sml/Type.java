package com.felixcool98.sml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.felixcool98.utility.multivaluemap.LinkedListMultiValueMap;

public class Type {
	private String name;
	
	private List<String> arguments = new ArrayList<>();
	private Map<String, String> argumentMap = new HashMap<>();
	
	private LinkedListMultiValueMap<String, Attribute> attributes = new LinkedListMultiValueMap<>();
	
	
	public Type(String name) {
		this.name = name;
	}
	
	
	//======================================================================
	// checks
	//======================================================================
	public boolean hasAttribute(String name) {
		return attributes.containsKey(name);
	}
	
	
	//======================================================================
	// setter / adding
	//======================================================================
	public void setName(String name) {
		this.name = name;
	}
	
	public void addAttribute(Attribute attribute) {
		attributes.put(attribute.getName(), attribute);
	}
	
	public void addArgument(String argument) {
		arguments.add(argument);
	}
	public void addArgument(String name, String value) {
		argumentMap.put(name, value);
	}
	
	
	//======================================================================
	// removing / clearing
	//======================================================================
	public void clearAttributes() {
		attributes.clear();
	}
	
	public void clearArguments() {
		arguments.clear();
		argumentMap.clear();
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	
	public String getName() {
		return name;
	}
	
	//arguments
	
	public List<String> getArguments(){
		return new ArrayList<>(arguments);
	}
	public String getArgument(int index) {
		return arguments.get(index);
	}
	public String getArgument() {
		return getArgument(0);
	}
	public String getArgument(String name) {
		return argumentMap.get(name);
	}
	public Set<Entry<String, String>> getArgumentMapEntries() {
		return argumentMap.entrySet();
	}
	
	//attributes
	
	public Attribute getAttribute(String name) {
		if(!hasAttribute(name))
			return null;
		
		return getAttributes(name).getFirst();
	}
	public LinkedList<Attribute> getAttributes(String name){
		return attributes.get(name);
	}
	public List<Attribute> getAttributes(){
		return attributes.values();
	}
	
	
	//======================================================================
	// overrides
	//======================================================================
	@Override
	public String toString() {
		return "Type " + getName();
	}
	public String toFormatString() {
		String text = getName();
		
		for(String argument : getArguments()) {
			text += "  " + argument;
		}
		for(Entry<String, String> entry : getArgumentMapEntries()) {
			text += "  " + entry.getKey() + " = " + entry.getValue();
		}
		
		for(Attribute attribute : attributes.values()) {
			text += System.lineSeparator() + attribute.toFormatString();
		}
		
		return text;
	}
}
