package sml;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Files;
import com.badlogic.gdx.files.FileHandle;
import com.felixcool98.sml.Attribute;
import com.felixcool98.sml.SMLReader;
import com.felixcool98.sml.Type;

public class SMLReaderTests {

	@Test
	public void test() {
		Gdx.files = new Lwjgl3Files();
		
		FileHandle handle = Gdx.files.local("blocks.mod");
		
		SMLReader reader = new SMLReader(true, 10);
		
		List<Type> types = reader.parse(handle.reader());
		
		for(Type type : types) {
			System.out.println(type.toFormatString());
			System.out.println();
		}
		
		assertEquals(5, types.size());
		
		assertEquals(1000.5f, types.get(1).getAttribute("breakable").getFloat("health", 0.5f), 0.0001f);
		
		assertEquals("other", types.get(3).getName());
		assertEquals("other obj", types.get(4).getName());
		
		assertEquals("t/e/st", types.get(3).getArgument());
		assertEquals("test 2", types.get(4).getArgument());
		assertEquals(2, types.get(2).getAttribute("multiple").getAttributes("a").size());
		
		for(Attribute attribute : types.get(2).getAttribute("multiple").getAttributes("a")) {
			assertTrue(attribute.hasAttribute("b"));
			assertTrue(attribute.hasAttribute("c"));
			assertTrue(attribute.hasAttribute("d"));
			assertEquals(3, attribute.getAttributes().size());
		}
		
		System.out.println(types.get(2).getAttribute("image").getText());
	}
}
