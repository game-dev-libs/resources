package sml;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Files;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.felixcool98.sml.SMLReader;
import com.felixcool98.sml.Type;
import com.felixcool98.sml.XMLConverter;

public class ConverterTest {

	@Test
	public void test() {
		Gdx.files = new Lwjgl3Files();
		
		FileHandle handle = Gdx.files.local("blocks.mod");
		
		SMLReader reader = new SMLReader(true, 10);
		
		List<Type> types = reader.parse(handle.reader());
		
		for(Type type : types) {
			System.out.println(type.toFormatString());
			System.out.println();
		}
		
		assertEquals(5, types.size());
		
		XMLConverter converter = new XMLConverter();
		
		Element element = converter.toXML(types, "blocks");
		
		System.out.println(element.toString());
	}

}
