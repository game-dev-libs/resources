package sml;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.sml.Attribute;

public class AttributeTest {

	@Test
	public void test() {
		Attribute attribute = new Attribute("");
		attribute = new Attribute("", attribute);
		
		assertEquals(1, attribute.getDepth());
		
		attribute = new Attribute("", attribute);
		
		assertEquals(2, attribute.getDepth());
	}

}
